const jogadoresJSONPath = "js/jogadores.json";
let jogadores;

async function fetchJogadores() {
  await fetch(jogadoresJSONPath, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  })
    .then((res) => res.json())
    .then((data) => {
      jogadores = data;
    });
}

function insertJogador(jogador) {
  let containerDiv = document.getElementById("container");

  const newPlayerCard = document.createElement("div");
  newPlayerCard.setAttribute("class", "playerCard");

  const newPlayerName = document.createElement("div");
  newPlayerName.setAttribute("class", "playerName");
  newPlayerName.textContent = jogador.nome.toUpperCase();

  const imgGroup = document.createElement("div");
  imgGroup.setAttribute("class", "imgGroup");

  const newPlayerPicture = document.createElement("img");
  newPlayerPicture.setAttribute("class", "playerPicture");
  newPlayerPicture.setAttribute("id", jogador.nome);
  newPlayerPicture.setAttribute("src", jogador.imagem);

  const newCupAndFieldImgGroup = document.createElement("div");
  newCupAndFieldImgGroup.setAttribute("class", "cupAndFieldImgGroup");

  const newCupLogo = document.createElement("img");
  newCupLogo.setAttribute("class", "cupLogo");
  newCupLogo.setAttribute("src", "images/world_cup_logo.png");

  const newFieldPosition = document.createElement("img");
  newFieldPosition.setAttribute("class", "fieldPosition");
  switch (jogador.posicao) {
    case "Goleiro":
    case "Zagueiro":
      newFieldPosition.setAttribute("src", "images/defesa.png");
      break;
    case "Lateral":
    case "Volante":
    case "Meia":
      newFieldPosition.setAttribute("src", "images/meio.png");
      break;
    default:
      newFieldPosition.setAttribute("src", "images/ataque.png");
  }

  const newTeamSquare = document.createElement("div");
  newTeamSquare.setAttribute("class", "teamSquare");

  const newTeamName = document.createElement("div");
  newTeamName.setAttribute("class", "teamName");
  newTeamName.textContent = "BOTA";

  const newBotafogoLogo = document.createElement("img");
  newBotafogoLogo.setAttribute("class", "botafogoLogo");
  newBotafogoLogo.setAttribute("src", "images/botafogo.png");

  const newPlayerBirthDay = document.createElement("div");
  newPlayerBirthDay.setAttribute("class", "playerBirthDay");
  newPlayerBirthDay.textContent = jogador.nascimento
    .slice(0, 10)
    .replaceAll("/", "-");

  newCupAndFieldImgGroup.appendChild(newCupLogo);
  newCupAndFieldImgGroup.appendChild(newFieldPosition);

  newTeamSquare.appendChild(newTeamName);
  newTeamSquare.appendChild(newBotafogoLogo);

  imgGroup.appendChild(newCupAndFieldImgGroup);
  imgGroup.appendChild(newPlayerPicture);
  imgGroup.appendChild(newTeamSquare);

  newPlayerCard.appendChild(imgGroup);
  newPlayerCard.appendChild(newPlayerName);
  newPlayerCard.appendChild(newPlayerBirthDay);
  newPlayerCard.addEventListener(
    "click",
    function () {
      callPlayerInfoPage(jogador);
    },
    false
  );

  containerDiv.appendChild(newPlayerCard);
}

function callPlayerInfoPage(jogador) {
  localStorage.removeItem("jogador");
  localStorage.setItem("jogador", JSON.stringify(jogador));

  window.location.href += "playerPage.html";
}

function fillPositions(jogadores) {
  const uniquePositions = [
    ...new Set(jogadores.map((jogador) => jogador.posicao)),
  ];

  uniquePositions.forEach(function (position) {
    const positionElement = document.createElement("option");
    positionElement.value = position;
    positionElement.text = position;

    document.getElementById("posicao_select").appendChild(positionElement);
  });
}

function UpdatePlayers() {
  document.getElementById("container").replaceChildren();

  const selectedPosition = document.getElementById("posicao_select").value;
  if (selectedPosition === "Todas") {
    jogadores.forEach(insertJogador);
  } else {
    jogadores
      .filter((jogador) => jogador.posicao === selectedPosition)
      .forEach(insertJogador);
  }
}

window.onload = async function () {
  await fetchJogadores();
  fillPositions(jogadores);

  jogadores.forEach(insertJogador);
};
