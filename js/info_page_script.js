let jogador;

function GeneratePage() {
  document.getElementById("title").textContent = jogador.nome;

  const playerImgGradient = document.createElement("div");
  playerImgGradient.setAttribute("id", "playerImgGradient");
  document.getElementById("imgWrap").appendChild(playerImgGradient);

  const playerImg = document.createElement("img");
  playerImg.setAttribute("src", jogador.imagem);
  document.getElementById("playerImgGradient").appendChild(playerImg);

  const imgOverlay = document.createElement("div");
  imgOverlay.setAttribute("id", "imgOverlay");
  document.getElementById("imgWrap").appendChild(imgOverlay);

  const playerName = document.createElement("div");
  playerName.setAttribute("id", "playerName");
  playerName.textContent = jogador.nome;
  document.getElementById("imgOverlay").appendChild(playerName);

  const botogoLogo = document.createElement("img");
  botogoLogo.setAttribute("src", "images/botafogo.png");
  botogoLogo.setAttribute("id", "botafogoLogo");
  document.getElementById("imgOverlay").appendChild(botogoLogo);

  const detailsContainer = document.createElement("div");
  detailsContainer.setAttribute("id", "detailsContainer");
  document.getElementById("container").appendChild(detailsContainer);

  const posicao = document.createElement("div");
  posicao.setAttribute("id", "posicao");
  posicao.setAttribute("class", "details");
  posicao.textContent = "Posição: " + jogador.posicao;
  document.getElementById("detailsContainer").appendChild(posicao);

  const height = document.createElement("div");
  height.setAttribute("id", "height");
  height.setAttribute("class", "details");
  height.textContent = "Altura: " + jogador.altura_peso.slice(0, 6);
  document.getElementById("detailsContainer").appendChild(height);

  const dateOfBirth = document.createElement("div");
  dateOfBirth.setAttribute("id", "dateOfBirth");
  dateOfBirth.setAttribute("class", "details");
  dateOfBirth.textContent =
    "Nascimento: " + jogador.nascimento.slice(0, 10).replaceAll("/", "-");
  document.getElementById("detailsContainer").appendChild(dateOfBirth);

  const descriptionContainer = document.createElement("div");
  descriptionContainer.setAttribute("id", "descriptionContainer");
  document.getElementById("container").appendChild(descriptionContainer);

  const descriptionHeader = document.createElement("div");
  descriptionHeader.setAttribute("id", "descriptionHeader");
  descriptionHeader.textContent = "Descrição";
  document
    .getElementById("descriptionContainer")
    .appendChild(descriptionHeader);

  const descriptionBody = document.createElement("div");
  descriptionBody.setAttribute("id", "descriptionBody");
  descriptionBody.textContent = jogador.descricao;
  document.getElementById("descriptionContainer").appendChild(descriptionBody);
}

window.onload = function () {
  jogador = JSON.parse(localStorage.getItem("jogador"));

  GeneratePage();
};
